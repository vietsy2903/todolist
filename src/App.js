import React, {Component} from 'react';
import './App.css';

import TaskForm from './components/taskForm';
import Control from './components/Control';
import TaskList from './components/TaskList';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            isDisplayForm: false,
            taskEdit: null,
            filter: {
                name: '',
                status: -1
            },
            keyword: '',
            sortBy: 'name',
            sortVal: 1
        };
    }

    componentWillMount(){
        if(localStorage && localStorage.getItem('tasks')){
            var tasks = JSON.parse(localStorage.getItem('tasks'));
            this.setState({ 
                tasks: tasks
            });
        }
    }

    s4(){
        return Math.floor((1 + Math.random())* 0x10000).toString(16).substring(1);
    }
    generateID(){
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4() + this.s4();
    }

    showTaskForm = () => {
        if(this.state.isDisplayForm && this.state.taskEdit !== null){
            this.setState({
                isDisplayForm: true,
                taskEdit: null
            });
        } else{
            this.setState({
                isDisplayForm: !this.state.isDisplayForm,
                taskEdit: null
            });
        }
    }

    onShowForm = () => {
        this.setState({
            isDisplayForm: true
        });
    }

    onCloseForm = () => {
        this.setState({
            isDisplayForm: false
        });
    }

    onSubmit = (data) => {
        var tasks = this.state.tasks;
        if(data.id === ''){
            data.id = this.generateID();
            tasks.push(data);
        } else{
            // edit data
            var index = this.findIndex(data.id);
            tasks[index] = data;
        }
        
        this.setState({
            tasks: tasks,
            taskEdit: null
        });
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    onUpdateState = (id) => {
        var tasks = this.state.tasks;
        var index = this.findIndex(id);
        if(index !== -1){
            tasks[index].status = !tasks[index].status;
            this.setState({
                tasks: tasks
            });
            localStorage.setItem('tasks', JSON.stringify(tasks));
        }
    }

    findIndex = (id) =>{
        var tasks = this.state.tasks;
        var result = -1;
        tasks.forEach((task, index) =>{
            if(task.id === id){
                result = index;
            }
        });
        return result;
    }

    onDelete = (id) =>{
        var tasks = this.state.tasks;
        var index = this.findIndex(id);
        if(index !== -1){
            tasks.splice(index, 1);
            this.setState({
                tasks: tasks
            });
            localStorage.setItem('tasks', JSON.stringify(tasks));
        }
        this.onCloseForm();
    }

    onUpdate = (id) =>{
        var tasks = this.state.tasks;
        var index = this.findIndex(id);
        var taskEdit = tasks[index];
        this.setState({
            taskEdit: taskEdit
        });
        this.onShowForm();
        //console.log(taskEdit);
    }

    onFilter = (filterName, filterStatus) =>{

        filterStatus = parseInt(filterStatus, 10);
        this.setState({
            filter: {
                name: filterName.toLowerCase(),
                status: filterStatus
            }
        });
    }

    onSearch = (keyword) =>{
        this.setState({
            keyword: keyword
        });
    }

    onSort = (sortBy, sortVal) =>{
        this.setState({
            sortBy: sortBy,
            sortVal: sortVal
        });
    }

    render() {
        var {tasks, isDisplayForm, taskEdit, filter, keyword, sortBy, sortVal} = this.state;
        if(filter){
            if(filter.name){
                tasks = tasks.filter((task) =>{
                    return task.name.toLowerCase().indexOf(filter.name) !== -1;
                });
            }
            tasks = tasks.filter((task) =>{
                if(filter.status === -1){
                    return task;
                } else{
                    return task.status === (filter.status === 1 ? true : false);
                }
            });
        }
        if(keyword){
            tasks = tasks.filter((task) =>{
                return task.name.toLowerCase().indexOf(keyword) !== -1;
            });
        }
        if(sortBy === 'name'){
            tasks.sort((a, b) =>{
                if(a.name > b.name){
                    return sortVal;
                } else if(a.name < b.name){
                    return -sortVal;
                } else{
                    return 0;
                }
            });
        }else{
             tasks.sort((a, b) =>{
                if(a.status > b.status){
                    return -sortVal;
                } else if(a.status < b.status){
                    return sortVal;
                } else{
                    return 0;
                }
            });
        }
        var elmTaskForm = isDisplayForm ? <TaskForm onSubmit={this.onSubmit} onCloseForm={this.onCloseForm} task={taskEdit} /> : '';
        return(
            <div className="todolist">
                <h1 className="text-center p-4 mb-3">Danh Sách Công Việc</h1>
                <div className="container">
                    <div className="row">
                        <div className={isDisplayForm ? 'col-md-4' : ''}>
                           {elmTaskForm} 
                        </div>
                        <div className={isDisplayForm ? 'col-md-8' : 'col-md-12'}>
                            <button type="button" className="btn btn-primary mb-3" onClick={this.showTaskForm}><i className="fa fa-plus"></i>Thêm Công Việc</button>
                            {/*Search And Sort*/}
                            <Control onSearch={this.onSearch} onSort={this.onSort} sortBy={sortBy} sortVal={sortVal} />

                            {/*List*/}
                            <TaskList tasks={tasks} onUpdateState={this.onUpdateState} onDelete={this.onDelete} onUpdate={this.onUpdate} onFilter={this.onFilter} />

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
