import React, {Component} from 'react';

class TaskForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            status: false
        };
        this.onHandelChange = this.onHandelChange.bind(this);
    }

    componentWillMount(){
        if(this.props.task){
            this.setState({
                id: this.props.task.id,
                name: this.props.task.name,
                status: this.props.task.status
            });
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.task){
            this.setState({
                id: nextProps.task.id,
                name: nextProps.task.name,
                status: nextProps.task.status
            });
        } else if(!nextProps.task){
            this.setState({
                id: '',
                name: '',
                status: false
            });
        }
    }

    onCloseForm = () => {
        this.props.onCloseForm();
    }

    onHandelChange(event){
        var target = event.target;
        var name = target.name;
        var value = target.value;
        if(name === 'status'){
            value = target.value === 'true' ? true : false;
        }
        this.setState({
            [name] : value
        });
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.state);
        // Cancel and Close Form
        this.onClear();
        this.onCloseForm();
    }

    onClear = () => {
        this.setState({
            name: '',
            status: false
        });
    }

    render() {
        var {id} = this.state;
        return(
            <div className="card">
                <div className="card-header bg-primary c-title01">{id !== '' ? 'Cập Nhật Công Việc' : 'Thêm Công Việc'}<i onClick={this.onCloseForm} className="fa fa-window-close-o"></i></div>
                <div className="card-body">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label>Tên:</label>
                            <input type="text" className="form-control" name="name" value={this.state.name} onChange={this.onHandelChange} />
                        </div>
                        <div className="form-group">
                            <label>Trạng Thái:</label>
                            <select className="form-control" name="status" value={this.state.status} onChange={this.onHandelChange}>
                                <option value={true}>Kích Hoạt</option>
                                <option value={false}>Ẩn</option>
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary mr-3"><i className="fa fa-floppy-o"></i> Lưu</button>
                        <button type="button" className="btn btn-danger" onClick={this.onClear}><i className="fa fa-times"></i> Hủy</button>                        
                    </form>
                </div>
            </div>
        );
    }
}

export default TaskForm;