import React, {Component} from 'react';

class TaskItem extends Component {

    onUpdateState = () =>{
        this.props.onUpdateState(this.props.task.id);
    }
    onDelete = () =>{
        this.props.onDelete(this.props.task.id);
    }

    onUpdate = () =>{
        this.props.onUpdate(this.props.task.id);
    }

    render() {
        var { task, index } = this.props;
        return(
            <tr>
                <td className="text-center">{index + 1}</td>
                <td>{task.name}</td>
                <td className="text-center"><span className={task.status === true ? 'badge w-50 p-2 badge-success' : 'badge w-50 p-2 badge-danger'} onClick={this.onUpdateState}>{task.status ===true ? 'Kích Hoạt' : 'Ẩn'}</span></td>
                <td>
                    <button type="button" className="btn btn-outline-warning mr-3" onClick={this.onUpdate}>Edit</button>
                    <button type="button" className="btn btn-outline-danger" onClick={this.onDelete}>Delete</button>
                </td>
            </tr>
        );
    }
}

export default TaskItem;