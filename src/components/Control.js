import React, {Component} from 'react';
import Search from './Search';
import Sort from './Sort';

class Control extends Component {
    render() {
        return(
            <div className="row mb-3">
                {/*Search*/}
                <Search onSearch={this.props.onSearch} />
                {/*Sort*/}
                <Sort onSort={this.props.onSort} sortBy={this.props.sortBy} sortVal={this.props.sortVal} />
            </div>
        );
    }
}

export default Control;