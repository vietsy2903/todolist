import React, {Component} from 'react';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {            
            keyword: ''
        };
    }

    onChange = (event) =>{
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name] : value
        });
    }

    onSearch = () =>{
        this.props.onSearch(this.state.keyword);
    }

    render() {
        return(
            <div className="col-md-6">
                <div className="input-group">
                    <input type="text" className="form-control" name="keyword" value={this.state.keyword} onChange={this.onChange} placeholder="Nhập Từ Khóa..." />
                    <div className="input-group-prepend">
                        <button className="btn btn-outline-primary" type="button" onClick={this.onSearch}>Search</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Search;