import React, {Component} from 'react';

class Sort extends Component {
    onClick = (sortBy, sortVal) =>{
        this.props.onSort(sortBy, sortVal);
    }
    render() {
        return(
            <div className="col-md-6">
                <div className="dropdown">
                    <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Sắp Xếp
                    </button>
                    <ul className="dropdown-menu">
                        <li className={this.props.sortBy === 'name' && this.props.sortVal === 1 ? 'dropdown-item is-active' : 'dropdown-item'} onClick={() => this.onClick('name', 1)}><i className="fa fa-sort-alpha-asc mr-4"></i>Tên A -> Z</li>
                        <li className={this.props.sortBy === 'name' && this.props.sortVal === -1 ? 'dropdown-item is-active' : 'dropdown-item'} onClick={() => this.onClick('name', -1)}><i className="fa fa-sort-alpha-desc mr-4"></i>Tên Z -> A</li>
                        <li className={this.props.sortBy === 'status' && this.props.sortVal === 1 ? 'dropdown-item is-active' : 'dropdown-item'} onClick={() => this.onClick('status', 1)}>Trạng Thái Kích Hoạt</li>
                        <li className={this.props.sortBy === 'status' && this.props.sortVal === -1 ? 'dropdown-item is-active' : 'dropdown-item'} onClick={() => this.onClick('status', -1)}>Trạng Thái Ẩn</li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Sort;